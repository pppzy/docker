# docker

#### 项目介绍
docker scripts.

#### 任务历程

1. 创建一个能够ssh登陆的docker image 并且运行
    ```shell
    docker login
    # user name is different from email address
    # default login to docker hub, but can specify to other
    docker pull rastasheep/ubuntu-sshd
    # run one
    docker run -d -P --name testssd rastasheep/ubuntu-sshd
    # -P publish all exposed port to random port
    # -d run container in background and print container ID
    # --name assign a name to the container
    docker port testssd 22
    # print the random port that assgined
    ```
    经过上面的步骤，一个ubuntu sshd服务器已经在本地运行
    ```shell
    ssh root@localhost -p 49154
    # password in dockerfile is `root`
    scp -P 49154 somefile root@localshot
    # copy somefile to the server
    docker cp somefile testssd:/path/to/dir
    ```
    上面的服务器如果需要更改密码
    ```shell
    docker exec -ti testssd password
    # -t --tty allocate a pseudo-tty
    # -i --interactive keep stdin open even if not attached
    docker cp publickey testssd:/root/.ssh/authorized_keys
    docker exec testssd chown root:root /root/.ssh/authorized_keys
    ```

    ```Dockerfile
    FROM       ubuntu:12.04
    MAINTAINER Aleksandar Diklic "https://github.com/rastasheep"

    RUN apt-get update

    RUN apt-get install -y openssh-server
    RUN mkdir /var/run/sshd

    RUN echo 'root:root' |chpasswd

    RUN sed -ri 's/^#?PermitRootLogin\s+.*/PermitRootLogin yes/' /etc/ssh/sshd_config
    RUN sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config

    RUN mkdir /root/.ssh

    RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

    EXPOSE 22

    CMD    ["/usr/sbin/sshd", "-D"]
    ```
    上面是直接使用docker pull过来的景象，也可以自己本地build
    ```shell
    docker build -t name:tag .
    # build image with name and tag. default Dockerfile
    ```

1. 本地registry
    [docker distribution](https://github.com/docker/distribution/blob/master/Dockerfile)
    ```Dockerfile
    FROM golang:1.10-alpine

    ENV DISTRIBUTION_DIR /go/src/github.com/docker/distribution
    ENV DOCKER_BUILDTAGS include_oss include_gcs

    ARG GOOS=linux
    ARG GOARCH=amd64

    RUN set -ex \
        && apk add --no-cache make git

    WORKDIR $DISTRIBUTION_DIR
    COPY . $DISTRIBUTION_DIR
    COPY cmd/registry/config-dev.yml /etc/docker/registry/config.yml

    RUN make PREFIX=/go clean binaries

    VOLUME ["/var/lib/registry"]
    EXPOSE 5000
    ENTRYPOINT ["registry"]
    CMD ["serve", "/etc/docker/registry/config.yml"]

    ```

1. windows 10 container 关机重新启动之后不能正确启动
   - 这个是win10更新的新功能引发的，win10会默认启动关机之前的进程```shutdown -s -t 0```
   - docker container stop container name 
