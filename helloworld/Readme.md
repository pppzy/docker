# 跟随docker doc的练习部分


## Build第一个镜像
    1. 建立的容器
    2. 推送容器到registry
    3. 运行容器，-d或者单独运行

## 初始化
    ```shell
    docker swarm init
    #>Swarm initialized: current node (isjcso9wccxwwtfbwp69anass) is now a manager.

    #>To add a worker to this swarm, run the following command:

    #>docker swarm join --token SWMTKN-1-1nwyczlz7nisiqhepzoyegpaq5govl6p1ih117k03a4kyi4nxe-4zet8vyws5to8cksgifh1vmwg 192.168.65.3:2377

    #>To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

    ```
    运行新的负载均衡的应用
    ```shell
    docker stack deploy -c docker-compose.yml getstartedlab
    #
    docker stack ps getstartedlab
    # 这里删除不会影响在运行的容器
    docker stack rm getstartedlab
    #
    docker swarm leave --force
    ```
    
## docker machine
    windows 10需要使用hyper-v功能，这个功能不是默认安装的
    ```powershell
    # Install only the PowerShell module
    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-Management-PowerShell

    # Install the Hyper-V management tool pack (Hyper-V Manager and the Hyper-V PowerShell module)
    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-Tools-All

    # Install the entire Hyper-V stack (hypervisor, services, and tools)
    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V-All
    ```
    下面是全部的命令
    ```shell
    docker-machine create --driver virtualbox myvm1 # 创建 VM（Mac、Win7、Linux）
    docker-machine create -d hyperv --hyperv-virtual-switch "myswitch" myvm1 # Win10
    docker-machine env myvm1                # 查看有关节点的基本信息
    docker-machine ssh myvm1 "docker node ls"         # 列出 swarm 中的节点
    docker-machine ssh myvm1 "docker node inspect <node ID>"        # 检查节点
    docker-machine ssh myvm1 "docker swarm join-token -q worker"   # 查看加入令牌
    docker-machine ssh myvm1   # 打开与 VM 的 SSH 会话；输入“exit”以结束会话
    docker-machine ssh myvm2 "docker swarm leave"  # 使工作节点退出 swarm
    docker-machine ssh myvm1 "docker swarm leave -f" # 使主节点退出，终止 swarm
    docker-machine start myvm1            # 启动当前未运行的 VM
    docker-machine stop $(docker-machine ls -q)               # 停止所有正在运行的 VM
    docker-machine rm $(docker-machine ls -q) # 删除所有 VM 及其磁盘镜像
    docker-machine scp docker-compose.yml myvm1:~     # 将文件复制到节点的主目录
    docker-machine ssh myvm1 "docker stack deploy -c <file> <app>"   # 部署应用
    ```
